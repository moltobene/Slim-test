<?php

/**
 * 	Classe per fare le connessioni e query a un Database mysql 
 * 
 *  ---- Andrea Provenzale ----
 */
class DB {

	protected $user    = 'root';
	protected $passwd  = 'root';
	protected $hostna  = 'localhost';
	protected $dbname  = 'dsign';

	public $link;
	public $result;
	public $errore  = "<div style=\"width:500px;padding:5px; border:solid #ccc 1px;\"><span style=\"color:#ff0000;font-size:18px;font-weight:bold;\">Class DB error: </span><br>";
	
	/**
	 *	Costruttore crea la connessione al db
	**/
	function __construct() {

		if( ! ( $this->link = @mysqli_connect($this->hostna,$this->user,$this->passwd,$this->dbname) ) ){
			$error = "impossibile connettersi al database controllare dati di accesso";
			echo $this->errore.$error."</div>";

			exit;
		}
		else
			mysqli_set_charset ( $this->link , 'utf8' );
	}
	
	/**
	 *	funzione che esegue la query nel db
	 *  @param $query
	**/
	function query($sql) {
		if( ! ( $this->result = @mysqli_query($this->link,$sql) ) ){
			$error = "impossibile effettuare la query nel db controllare la sintassi<br><br>
					  <div style=\"color:#aa2200;border-bottom:solid #ccc 1px;font-size:14px;\">Query:</div>$sql
					  <div style=\"color:#aa2200;border-bottom:solid #ccc 1px;font-size:14px;\">mysql error:</div>".mysqli_error($this->link)."";
			echo $this->errore.$error."</div>";
			return false;
		}
		else
			return true;
	}
	
	/**
	 *	funzione per contare il numero delle righe restituite da una query 
	**/
	function num_rows() {
		return @mysqli_num_rows($this->result);
	}
	
	/**
	 *	funzione per avere un oggetto che abbia tutti i risultati della query 
	**/
	function fetch_object() {
		return @mysqli_fetch_object($this->result);
	}
	
	/**
	 *	funzione per avere un array che abbia tutti i risultati della query 
	**/
	function fetch_array() {
		return @mysqli_fetch_array($this->result);
	}

	/**
	 *	funzione per chiudere la connessione al db 
	**/
	function result(){
		// return mysql_result($this->result, 0);
		$identif = mysqli_fetch_array($this->result);
		$identif = $identif[0];
		return $identif;
		// return @mysqli_free_result($this->result);
	}

	/**
	 *	funzione per chiudere la connessione al db 
	**/
	function close(){
		mysqli_close($this->link);
	}

	// sanitize
	function sanitize($string){
		return strip_tags( mysqli_real_escape_string($this->link,$string));
	}

}

?>